FROM node:alpine
WORKDIR /src

COPY . .

RUN npm install
RUN npm prune --production

EXPOSE 8080

CMD ["npm", "start"]